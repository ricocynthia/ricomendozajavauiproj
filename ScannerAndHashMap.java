import java.util.Scanner;
import java.util.Map.Entry;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Main fuctionality of application
 * 
 * @author Cynthia Rico Mendoza
 */
public class ScannerAndHashMap {
    public static Scanner scanner;
    public static HashMap<String, Integer> hashMap = new HashMap<>();
    public static BufferedWriter writer;
    public static File countedWordsFile;
    public static int maxHashMapValue;
    public static int minHashMapValue;
    public static List<String> maxHashMapKeys = new ArrayList<>();
    public static List<String> minHashMapKeys = new ArrayList<>();

    /**
     * This construtor takes in the user file For testing functionality, I have
     * created my own file and haven't allowed the user to upload the file
     * themselves just yet.
     * 
     * @param fileName
     * @return hashMap - key = words, value = number of times words were counted
     * @throws FileNotFoundException
     * @throws IOException
     */
    public ScannerAndHashMap(File fileName) throws FileNotFoundException, IOException {
        // Begin to scan the file fileName
        File fileChoosenByUser = new File(fileName.getPath());
        scanFile(fileChoosenByUser.getPath(), fileChoosenByUser);
    }

    /**
     * This method creates a scannner that will scan through the fileName A
     * try/catch block is thrown in case the file is not found - in that case the
     * program will exit.
     * 
     * @param fileName - This is the name of the file the scanner will be reading
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void assignScanner(File fileChoosenByUser) throws FileNotFoundException, IOException {
        try {
            scanner = new Scanner(fileChoosenByUser);
        } catch (Exception e) {
            System.out.println("Could not find file. Please try again. (Make sure you are upladong a text file.)");;
        }

    }

    /**
     * The method completes the functionality of scanning the text file
     * 
     * @param fileName - the name of the file the scanner will be scanning
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void scanFile(String filePath, File fileChoosenByUser) throws FileNotFoundException, IOException {
        assignScanner(fileChoosenByUser); // sets up scanner to scan fileName

        System.out.println("is the hashmap clear? " + checkIfHashMapIsClear());

        if (!checkIfHashMapIsClear()) {
            clear();
            countedWordsFile.delete();
            try {
                countedWordsFile.createNewFile();
            } catch (Exception e) {
                System.out.println("Couldn't create a new file....");
            }
        }

        // Begin to read the file
        int countWords = 0; // varibale for total number of words in file
        System.out.println("Currently scanning file:");
        // Boolean handleScannerHasNextLine = scanner.hasNextLine();
        while (scanner.hasNextLine()) { // scanning for each line
            while (scanner.hasNext()) { // scanning for each word
                String word = scanner.next(); // setting word in file as a string variable
                System.out.println(word);
                word = word.replaceAll("\\p{Punct}", ""); // ignoring all punctuations
                countWords++; // incrementing the total num of words variable
                addToHashMap(word);
            }

            // Breaking the while loop when there are no more words to scan
            if (!scanner.hasNext()) {
                break;
            }

        } // end outter while
        scanner.close();
        System.out.println("Done scanning file.");

        // Print hashMap
        System.out.println("Printing file to new file.");
        printHashMapToFile(countWords, fileChoosenByUser);

        System.out.println("is the hashmap clear? " + checkIfHashMapIsClear());

    }

    /**
     * This function returns the string of hashmap with no commas - commas are
     * replaced with a new line instead (better for readability)
     * 
     * @return string of hashmap
     */
    public static String getHashMapValues() {
        String hashMapString = hashMap.toString();
        hashMapString = hashMapString.replaceAll(",", "\n");

        return hashMapString;
    }

    /**
     * The method doesn't take anything in... The functionality of this method is to
     * print: 1) the total number of words in the file 2) total number of different
     * words in the file 3) the count of each time a different word is used in the
     * file
     * 
     * @param countWords
     * @throws FileNotFoundException
     */
    private static void printHashMapToFile(int countWords, File fileName) throws FileNotFoundException, IOException {
        /*
         * Just getting the name of file without extension Helpful for naming the file
         * "filenameOutput.txt"
         */
        String fileUploaded = fileName.getName();
        int pos = fileUploaded.lastIndexOf(".");
        if (pos > 0) {
            fileUploaded = fileUploaded.substring(0, pos);
        }

        File outputFile = new File(fileUploaded + "Output.txt");
        countedWordsFile = outputFile;

        writer = new BufferedWriter(new FileWriter(countedWordsFile.getName()));
        writer.write("The file " + fileName.getName()
                + " (which you uploaded) has the following information about the words in the file.");
        writer.newLine();

        if (hashMap.isEmpty()) {
            writer.write("map is empty");
        } else {
            writer.write("Total number of words in file: " + countWords);
            writer.newLine();
            writer.write("Total number of different words in file: " + hashMap.size());
            writer.newLine();
            writer.write(
                    "The following line will show the count of each time a word is used in file:\n" + getHashMapValues());
        }
        writer.close();

    }

    /**
     * The purpose of this method is to check add words to the hashMap from the
     * scanFile() and check whether that word already exists in the hashMap or not.
     * If the word does NOT exist in the hashMap, then add it to the hashMap with
     * the value of 1. If the word does exist, then call updateHashMap()
     * 
     * @param word - this string is from the text file being read from the
     *             scanFile()
     */
    public static void addToHashMap(String word) {
        if (!hashMap.containsKey(word)) { // if word does NOT exists in hashmap...
            int val = 1;
            hashMap.put(word, val);
        } else {
            updateHashMap(word);
        }
    }

    /**
     * This function handles the updating process of incrementing the value of the
     * word when it already exists in the hashMap
     * 
     * @param word - this string is from the text file being read from the
     *             scanFile()
     */
    public static void updateHashMap(String word) {
        int oldValue = hashMap.get(word); // getting the old value of the word
        int newValue = oldValue++; // incrementing the value - since the word has been found again in the file
        hashMap.replace(word, newValue, oldValue); // updating the hashMap key and value

    } // end updateHashMap

    /**
     * Returns the most popular word(s) in file
     * 
     * @return list (toString()) of most popular word(s)
     */
    public static String getMostPopularWords() {

        // Getting the highest used word in file
        maxHashMapValue = Collections.max(hashMap.values());

        // Creating a list to store the key(s) that have the same value as the
        for (Entry<String, Integer> entry : hashMap.entrySet()) {
            if (maxHashMapKeys.size() < 5) {
                if (entry.getValue() == maxHashMapValue) {
                    maxHashMapKeys.add(entry.getKey());
                }
            }
        }

        return maxHashMapKeys.toString();
    }

    /**
     * Returns the number for the amount of times the most popular word was used in
     * file
     * 
     * @return most popular word(s) count
     */
    public static int getMostPopularWordsValues() {
        return maxHashMapValue;
    }

    /**
     * Returns the total number of words in the file
     * 
     * @return total num of words
     */
    public static int getWordCount() {
        return hashMap.size();
    }

    /**
     * Returns the least popular word(s) used in file
     * 
     * @return list (toString()) of least popular words
     */
    public static String getLeastPopluarWords() {
        minHashMapValue = Collections.min(hashMap.values());

        // Creating a list to store the key(s) that have the same value as the
        // minHashMapValue
        for (Entry<String, Integer> entry : hashMap.entrySet()) {
            if (minHashMapKeys.size() < 5) {
                if (entry.getValue() == minHashMapValue) {
                    minHashMapKeys.add(entry.getKey());
                }
            }
        }
        return minHashMapKeys.toString();
    }

    /**
     * Returns the number for the amount of times the least popular word was used in file
     * 
     * @return least popular word count
     */
    public static int getLeastPopluarWordsValues() {
        return minHashMapValue;
    }

    /**
     * Clears all max/mins variable about the value and keys of hashmap, to help
     * handle when user uploads another file in application
     */
    public static void clear() throws IOException {
        // Clearing max and min list with keys
        maxHashMapKeys.clear();
        minHashMapKeys.clear();

        // Clearing HashMap
        hashMap.clear();
    }

    /**
     * Checking if hashMap is empty Helpful for GuiWindow so it can clear the text
     * area when needed
     * 
     * @return true/false
     */
    public static Boolean checkIfHashMapIsClear() {
        if (!hashMap.isEmpty()) {
            return false;
        }

        return true;

    }
}