import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        // Start up the GuiWindow
        GuiWindow theWindow = new GuiWindow();
        theWindow.setVisible(true);

    }
}