import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * The window with the ability to upload your own text file
 * 
 * @author Cynthia Rico Mendoza
 */
public class GuiWindow extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    private JLabel uploadFile = new JLabel("Upload file");
    private JLabel fileName = new JLabel("");
    private String promptDescriptionText = "Please upload a text file in order to see "
            + "the most/least popular words in the file. <br/>This application will also create a text file with the total "
            + "number of words, total number of different words, and the count of each word in the file.";
    private JLabel promptDescription = new JLabel(
            "<html> <div style='font-size: 13px'>" + promptDescriptionText + " </div> </html>");
    private JButton fileButton = new JButton("Which File?");
    private JTextArea fileText = new JTextArea();
    private static ScannerAndHashMap scanAndHash;

    /**
     * Creates a GuiWindow, positioning all the components.
     */
    public GuiWindow() throws FileNotFoundException, IOException {
        this.setTitle("Word Counter Application");
        this.setBounds(500, 100, 900, 700);
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.promptDescription.setBounds(10, 10, 700, 80);
        this.getContentPane().add(promptDescription);

        this.uploadFile.setBounds(10, 100, 300, 30);
        this.getContentPane().add(uploadFile);

        this.fileButton.setBounds(10, 125, 100, 30);
        this.getContentPane().add(fileButton);
        this.fileButton.addActionListener(this);

        this.fileName.setBounds(10, 145, 300, 30);
        this.getContentPane().add(fileName);
    }

    /**
     * Invokes a file chooser with a text filter and gets ready to print file
     * (fileNameOutput.txt) created based on the file user uploads
     * 
     * @throws IOException
     * @throws FileNotFoundException
     */
    private void theFileButtonHasBeenPushed() throws FileNotFoundException, IOException {
        updateTextAreaAndScrollUI(); // Checking if scanAndHash hashmap is empty

        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
        chooser.setCurrentDirectory(new java.io.File("."));
        int chooserSuccess = chooser.showOpenDialog(null);
        if (chooserSuccess == JFileChooser.APPROVE_OPTION) {
            File chosenFile = chooser.getSelectedFile();
            fileName.setText(chosenFile.getName());

            // Passing file to scanAndHash to scan and create output file
            scanAndHash = new ScannerAndHashMap(chosenFile);

            // print word count info to UI
            printWordCountInfo(chosenFile);

        } else {
            System.out.println("You hit cancel");
        }
    }

    /**
     * Appends text about the uploaded file to JTextArea This will show the user the
     * most/least popular words in the file, total number of words in file, and the
     * name of the output file with more information about the word count of the
     * uploaded file.
     * 
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void printWordCountInfo(File chosenFile) throws FileNotFoundException, IOException {
        // Prepping the JTextArea to appear on UI
        this.fileText.setEditable(false);
        this.fileText.setBounds(300, 100, 500, 300);
        this.getContentPane().add(fileText);

        // Getting most popular hashmap keys and value
        String hashMapMaxKey = scanAndHash.getMostPopularWords();
        int hashMapMaxValue = scanAndHash.getMostPopularWordsValues();

        // Getting least popular hashmap keys and value
        String hashMapMinKey = scanAndHash.getLeastPopluarWords();
        int hashMapMinValue = scanAndHash.getLeastPopluarWordsValues();

        // Adding most/lease popular hashmap keys/values to text area
        this.fileText.append("The file " + fileName.getText() + " has the following most/least popular words: " + "\n");
        this.fileText.append(hashMapMaxKey + "=" + hashMapMaxValue + "\n");
        this.fileText.append(hashMapMinKey + "=" + hashMapMinValue + "\n");
        this.fileText.append("\nThere are " + scanAndHash.getWordCount() + " total words in this file." + "\n");
        this.fileText.append("See " + scanAndHash.countedWordsFile + " file for more information about output file.");
    }

    /**
     * Callback responding to pushes on either button.
     * 
     * @param e The action event, including the button command.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("The action event is " + e);
        if (e.getActionCommand().equals("Which File?")) {
            try {
                this.theFileButtonHasBeenPushed();
            } catch (FileNotFoundException e1) {
                System.out.println("File not found.");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    /**
     * Checks if hashmap is clear is ScanAndHashMap, so it can reset the JTextArea
     * to an empty string to help prepare if user wants to updload another file
     */
    private void updateTextAreaAndScrollUI() {
        if (!scanAndHash.checkIfHashMapIsClear()) {
            this.fileText.setText("");
        }

    }

}