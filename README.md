What is the purpose of this project?
The purpose of this project is to create an application that includes a scanner that has the ability to read a text document.
While reading this text document, we need to make sure the scanner is able do the following:
- Count the total number of words
- Count the total number of different words
- Count each of the different words used (e.g if "school" is used three times in the text doc. the scanner should note that the word "school" was used 3 times.)

How will we be running this project?
This application will be developed in Java. Therefore, we will be running it on the command line or NetBeans -- whatever is your preference.

What is going to be included in the directory?
You will see three classes: (1) GUI, (2) Main, and (3) ScannerAndHashMap 